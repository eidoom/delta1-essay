#!/usr/bin/env bash

perl -pi -e "
    s|(by\|with) \((.*?), (\d{4})\)|\1 \2 (\3)|gm;
    s|\(([^0-9]*?), (\d{4})\) (showed\|posit)|\1 (\2) \3|gm;
    " $1
