# [delta1-essay](https://gitlab.com/eidoom/delta1-essay)

Since I have to submit this in a `.docx` form, I'll write it here in `.md` with the references in a `.bib` and use `pandoc` to compile it to `.txt` with citations handled by `citeproc`.
Then I can just copy the contents of the output files from `make -j` into their form.

Tested with [`pandoc`](https://pandoc.org/installing.html) 2.14.1.

Final version live [here](https://eidoom.github.io/post/delta1).
