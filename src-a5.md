My practice is mostly influenced by apprenticeship of observation [@lortie2020schoolteacher].
I have been fortunate in receiving a variety of teaching from three institutions---recognising both good and bad.
For example, as an undergraduate I was inspired by the unconventionally active and conceptual methods employed in my electrodynamics course and the related study by [@charlie] that I participated in.
While it has its utility, I find over-reliance on passive learning detrimental to engagement and long-term retention.
[K1,K2,K3]

To help adapt to virtual teaching, I attended meetings run by my department where I engaged in open discussions about how best to use the technology.
My peers and I also self-organised trial sessions to explore the online platforms before term started.
We have maintained regular "corridor discussions" to reflect on our teaching experiences together.
For example, this inspired the switch to breakout rooms.
[K4,K5]

As I mentioned in A4, I work closely with the lecturers of the module to iteratively improve how we present the module.
Early in the year, I ran sessions together with the first lecturer in the same virtual room.
While I later found breakout rooms to be superior, this provided a valuable opportunity for us to mutually peer observe [K5].
Through this, I gained further insight into the module's content and how to present these specific topics in a legible manner [K1].
We also held short retrospectives after lessons where we shared feedback on observation and identified students' weak points to be reinforced next time [K5].

In addition to informal conversations with students, which are useful for continuing feedback but may be an environment in which they are not comfortable sharing everything [V1], I distributed an anonymous online questionnaire [K4,K5].
I did this once the format of the workshop had stabilised, which was towards the end of the course, so its main utility will be in informing my teaching next year.
The students were highly perceptive in picking up on the discussed efforts I have made to improve their experience, and praised them.
Some comments indicated the level of guidance was high while others suggested it was too low; even arguments in either direction suggest to me that the level of challenge was set about right.
One point lacking was the ability to have one-on-one discussions so I will be sure to incorporate this option into future teaching.
With all forms of student feedback, I strove to act within the top steps of the Biggs model, adapting my teaching in response---"what the student does"---while not getting in the way of the student developing their own framework---"how the student manages what the student does" [@biggs11] [V3].
[K2,K3]

Early in my perusing of the Gateway to Academic Practice course, I discovered the Kolb-Lewin Learning Cycle [@kolb84].
This provided a productive framework to support my experiential learning in supporting others' learning.
My initial Planning drew on previous teaching experiences and early discussions.
I cover my actions in the Doing stage in A2 and A4.
Evaluation from myself, peers, and students formed the evidence on which to Reflect.
I like the Conceptualise step for its focus on reviewing reflection against literature and colleagues and the new insight this brings.
[V3]
