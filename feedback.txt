Summary Feedback

Reviewer 1
Thank you for providing this excellent reflective account of your teaching practice. It is clear that pedagogical ideas are embedded in your practice and you are developing in to an excellent practitioner.

Reviewer 2
A very good account of not only your pedagogic practice, but how you have used the literature to inform your transition of practice to an online environment. There are some highly innovative ideas in here, for example the virtual "corridor conversations", which show an adaptiveness and a commitment to values. The only aspect which could be improved upon is the role of personal reflection in improving practice - giving an indication of how effective you perceived these innovations to be.
