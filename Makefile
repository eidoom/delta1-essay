SRC=src.md
SRC1=src-a2.md
SRC2=src-a4.md
SRC3=src-a5.md
OUT=doc.txt
OUT1=a2.txt
OUT2=a4.txt
OUT3=a5.txt
BIB=refs.bib
CFG=conf.yaml
CSL=apa-no-ampersand.csl

.PHONY: all clean

all: ${OUT1} ${OUT2} ${OUT3} ${OUT}

${OUT1}: ${SRC1} ${CFG} ${BIB} ${CSL} wc1
	cp $< tmp1
	echo -e "\n# References" >>tmp1
	pandoc --defaults=$(word 2, $^) --bibliography=$(word 3, $^) --csl=$(word 4, $^) --output=$@ tmp1
	rm tmp1
	./fix.sh $@
	echo -en "\nWord count: " >>$@
	cat $(word 5, $^) >>$@

${OUT2}: ${SRC2} ${CFG} ${BIB} ${CSL} wc2
	cp $< tmp2
	echo -e "\n# References" >>tmp2
	pandoc --defaults=$(word 2, $^) --bibliography=$(word 3, $^) --csl=$(word 4, $^) --output=$@ tmp2
	rm tmp2
	./fix.sh $@
	echo -en "\nWord count: " >>$@
	cat $(word 5, $^) >>$@

${OUT3}: ${SRC3} ${CFG} ${BIB} ${CSL} wc3
	cp $< tmp3
	echo -e "\n# References" >>tmp3
	pandoc --defaults=$(word 2, $^) --bibliography=$(word 3, $^) --csl=$(word 4, $^) --output=$@ tmp3
	rm tmp3
	./fix.sh $@
	echo -en "\nWord count: " >>$@
	cat $(word 5, $^) >>$@

${OUT}: ${SRC} ${CFG} ${BIB} ${CSL}
	pandoc --defaults=$(word 2, $^) --bibliography=$(word 3, $^) --csl=$(word 4, $^) --output=$@ $<
	./fix.sh $@

doc.pdf: ${SRC} ${CFG} ${BIB} ${CSL}
	pandoc --defaults=$(word 2, $^) --bibliography=$(word 3, $^) --csl=$(word 4, $^) --to=pdf --output=$@ $<

${SRC}: ${SRC1} ${SRC2} ${SRC3} headings.txt
	echo -n "# " >$@
	sed -n '1{p;q}' $(word 4, $^) >>$@
	echo >>$@
	cat $< >>$@
	echo -en "\n# " >>$@
	sed -n '2{p;q}' $(word 4, $^) >>$@
	echo >>$@
	cat $(word 2, $^) >>$@
	echo -en "\n# " >>$@
	sed -n '3{p;q}' $(word 4, $^) >>$@
	echo >>$@
	cat $(word 3, $^) >>$@
	echo -e "\n# References" >>$@

wc1: ${SRC1}
	perl -p -e "s| ?\[.*?\]||g" $< | wc -w > $@

wc2: ${SRC2}
	perl -p -e "s| ?\[.*?\]||g" $< | wc -w > $@

wc3: ${SRC3}
	perl -p -e "s| ?\[.*?\]||g" $< | wc -w > $@

clean:
	git clean -fx
