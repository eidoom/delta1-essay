Due to coronavirus restrictions, my teaching this year is entirely online.
This presents a radically different learning environment for the module than what both I and students are used to.
The module lecturers and I work together to continuously evolve our delivery of the course as we adapt to the novel situation.

In my experience as a theoretical physics workshop demonstrator over the past few years, I have noticed some students will somewhat blindly jump into the mathematics of the problem sheet and end up floundering if they are not lucky.
I find starting the session with a holistic exposition of the worksheet helps the students to link the problems with lecture content and bridge over from previous workshops, and provides clear learning aims to guide them forward as strategised by [@AshmoreLyn2014Ltad] [V3].
In particular, this nurtures their independent research skills by hinting where to start reading when they get stuck [V4].
This helps achieve a fertile and mature educational context to their learning environment.
[K2,K3]

The most significant change I have made to the virtual setting since starting is to introduce breakout rooms.
This emulates the function of group tables in a physical setting.
We split the class in two randomly and I take one group while the current lecturer takes the other.
Feedback from the students (see A5) reveals that they feel far less peer anxiety in the smaller setting so are much happier to ask questions.
This agrees with my observation of dramatically increased student engagement [K5].
I also take advantage of anonymous polling technology to ask closed questions with inclusive participation [K2].
This is very helpful to gauge class progress.
Using a tablet with screenshare is vital to communicating mathematics.
[K4;V1]

As an educator, I take steps to present myself online in an approachable manner.
This is particularly important in the online environment as it can feel isolating for both students, as identified by [@isolation], and teachers [V3].
I keep my camera on at all times and keep my attention on the class to visually confirm my presence to the students.
I find that I am more comfortable and engaged in teaching while standing up so run the workshops from a standing desk.
I am careful to speak clearly and at a slower pace to ensure everyone can hear me regardless of audio equipment quality.
[K2]

Also to counter the digital disconnect, I make extra effort to cultivate a sense of belonging within the class.
Even during normal teaching, I have found that it is common for students in this field to experience a degree of imposter syndrome, also noted by [@imposter].
Therefore, in agreement with [@community], I view the feeling of community as essential [V3].
During the lessons, I maintain respectful and friendly relationships with the students, learning their names and starting conversations; this works well with the small class size of a dozen [K2;V1].
Outside the classroom, I keep an open door in answering email questions.
I have also led informal sessions about my research and postgraduate life within the department's virtual social space as well as encouraging students to attend an early career researcher conference hosted at Durham and a national women in physics conference.
[V2,V4]
