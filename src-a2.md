This year I am a workshop demonstrator in level four Particle Theory for MPhys Theoretical Physics.
At this highest level of the undergraduate degree, I hold that it is imperative to allow students the greatest possible autonomy while balancing against the support required to broach such an advanced topic.
The class is prescribed in the form of the students working though a problem sheet which we provide to them; beyond this, I foster an environment of active learning [@be91].
[@Freeman8410] showed that such student-centred learning is favourable in subjects like physics and in my personal experience is highly engaging and effective.
[K3;V3]

My interaction with the students is mostly discursive.
In the early minutes of the class, I give the initiative to the students to ask questions, inviting them to control their own learning.
In answering, I act as a facilitator of learning rather than a presenter of knowledge: nudging in the right direction rather than telling the answer, often by rephrasing the problem in a more accessible way [V1].
As the session progresses, I pose questions to the class in regular intervals and in answer to those of individuals.
I first assess students' understanding and bolster their confidence with lower-order questions, which also focus attention on salient points.
I am careful to allow a generous wait time for responses, as recommended by [@rowe1972], then prompt with closed questions if discussion stalls.
With knowledge established and momentum built, I let the students direct subsequent progression, guiding with higher-order questions if necessary.
Throughout, my questions are informed by my knowledge of the course's content and assessment and, with the module sharing its specialisation with my research, my practitioner's comprehension of the field [K1].
As discussed by [@BeckerLucindaM.2017Tihe], I always respond positively to answers, even if they are wrong, which I find encourages further interaction.
This is an intense learning state to engage in for an hour; the students respond well to a brief respite in the middle while I touch on the points of the next paragraph.
[@1982xi] SOLO taxonomy categorises students' level of engagement with course material, which I use to inform these teaching methods.
Students generally enter each class with a multistructural understanding of the relevant lecture content.
This structure aims to help students progress to the extended abstract level.
[K2,K3;V3]

Providing motivation to students is a critical role of the teacher.
[@doi:10.1080/03075079.2014.999319] posit that the teaching activities I described above serve to motivate through passing choice and control to the students as well as building belief in their own ability and self-efficacy.
Furthermore, I try to inspire students' interest by sharing my passion towards the field and giving insight into the context of what they are learning within cutting-edge research [K1].
I also indicate the relevance of the problems by showing how they fit into the module and their degree as a whole, as well as the applicability of the skills involved to future employment [V4].
This covers the key factors identified by [@Pintrich] for student motivation.
[V3]
